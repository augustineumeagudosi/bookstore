<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book_User extends Model
{
    protected $fillable = [
        'book_id',
        'user_id',
      ];
}
