<?php

namespace App\Http\Controllers;

use App\Book;
use App\Book_User;
use Illuminate\Http\Request;
use App\Http\Resources\BookResource;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $size = request('size') || 10;
        $page = $request->has('page') ? $request->query('page') : 1;
        //Cache here
        $books = Book::with('review')->orderBy('created_at', 'desc')->paginate(10);
       

        if ($books) {
            return BookResource::collection($books);
        }

        if (request('sortColumn') && request('sortColumn') == 'title') {
            $books = Book::with('review')->sortBy('title', request('sortDirection'))->paginate($page, $size)->get();
        }

        if (request('sortColumn') && request('sortColumn') == 'avg_review') {
            $books = Book::with('review')->sortBy('title', request('sortDirection'))->paginate($page, $size)->get();
        }

        if (request('title')) {
            $searchTerm = request('title');
            $books = Book::with('review')->where('title', 'LIKE', "%{$searchTerm}%")->paginate($page, $size)->get();
        }

        if (request('authors')) {
            $searchTerm = request('authors');
            $books = Book::with('review')->whereIn('author_id', $searchTerm)->paginate($page, $size)->get();
        }

        return BookResource::collection($books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->is_admin == 1) {

            return Book::create($request->all());
        } else {
            return response()->json('401: unauthorized access');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->is_admin == 1) {
            $this->validate($request, [
                'isbn' => 'required|digits:13|unique:books',
                'title' => 'required|string',
                'description' => 'required|string',
                'authors' => 'required',
            ]);
    
            $book = new Book;
            $book->ISBN = $request->input('isbn');
            $book->title = $request->input('title');
            $book->description = $request->input('description');
            $book->save();
            
            $authors = $request->input('author');            
            foreach ($authors as $author){
                $authorExist = User::find($author);
                if($authorExist){
                    $book_user = new Book_User;        
                    $book_user->book_id = $data->id;
                    $book_user->user_id = $request->input('authors');
                    $book_user->save();
                }
            }    
            return response()->json(201);
        } else {
            return response()->json('401: unauthorized access');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new BookResource(Book::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // RUN VALIDTION HERE

        if (auth()->user()->is_admin == 1) {
            $book = Book::findOrFail($id);

            $book->update($request->all());

            return new BookResource($book);
        } else {
            return response()->json('401: unauthorized access');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->is_admin == 1) {
            $book = Book::findOrFail($id);
            $book->delete();

            return response()->json(204);
        } else {
            
            return response()->json('401: unauthorized access');
        }
    }
}
