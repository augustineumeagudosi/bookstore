<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Book;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $books = DB::table('books')->orderBy('created_at', 'asc')->paginate(2);
        $data = array(
            'books' => $books,
        );
        return view('home')->with($data);
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        $books = DB::table('books')->orderBy('created_at', 'asc')->paginate(2);
        $data = array(
            'books' => $books,
        );
        return view('adminHome')->with($data);
    }
}
