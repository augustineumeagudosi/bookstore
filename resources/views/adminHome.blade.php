@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="table-responsive" style='margin-top:10%'>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">ISBN</th>
                        <th scope="col">Book Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Average Review</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($books as $book)
                        <tr>
                        <th>{{$book->ISBN}}</th>
                        <td>{{$book->title}}</td>
                        <td>{{$book->description}}</td>
                        <td>{{$book->avg_review}}</td>
                        <td>
                            <button type="button" class="btn btn-primary" style="color:#fff">Update</button>
                            <button type="button" class="btn btn-danger" style="color:#fff">Delete</button>
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$books->links()}}
            </div>
        </div>
    </div>
</div>
@endsection